# Red Light Stimulated RAS Signaling RNA-Seq

## View the analysis

The analysis and results can be viewed in the RMarkdown notebook [`diff_exp_analysis.nb.html`](https://bitbucket.org/princeton_genomics/ltanner_glycolosis_flux_rnaseq/src/195f20ef7e02f9a6708f6deec92f74e7bfbf0ff7/output/diff_exp_analysis.nb.html).

## Running the analysis

1.  Run Galaxy workflow to obtain count files.
	The workflow and count files output are stored in this repository.

2.	Use `package_results.sh` to generate a tarball with updated results.
	Analysis code is in `diff_exp_analysis.Rmd`, output stored in `output` directory.


